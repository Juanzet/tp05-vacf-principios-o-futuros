extends CharacterBody2D

@onready var _focus = $Focus
@onready var animation_player = $AnimationPlayer
@onready var progress_bar = $ProgressBar

@onready var MAX_HEALTH: float = 7

#var morosCounter: int
#var dead: bool = false

var health: float = 7:
	set(value):
		health = value
		_update_progess_bar()
		_play_animation()

func _process(delta):
	if health <= 0:
		Global.morosCount += 1
		get_parent().remove_child(self)
		#self.visible = false
		
func _update_progess_bar():
	progress_bar.value = (health/MAX_HEALTH) * 100

func _play_animation():
	animation_player.play("hurt")

func focus():
	_focus.show()

func unfocus():
	_focus.hide()

func take_damage(value):
	health -= value
	print(Global.morosCount)
	#if health <= value:
		#morosCounter += 1
	#else:
		#return 
