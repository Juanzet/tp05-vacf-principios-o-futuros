extends Control

@export_file("*.json") var d_file
@export_file("*.json") var d_file1

signal dialogue_finished

var _file: int
var dialogue = []
var current_dialogue_id = 0
var d_active = false

func _ready():
	#$NinePatchRect.visible = false
	print($NinePatchRect.visible)
	$NinePatchRect.hide()
	#start()

func start():
	if d_active:
		return
	d_active = true
	$NinePatchRect.show()
	dialogue = load_dialogue()
	current_dialogue_id = -1
	next_script()
	
func load_dialogue():
	#if Input.is_action_just_pressed("chat"):
	if d_active:
		#match _file:
			#0:
				#FileAccess.open("res://Dialogue/Worker_Dialogue1.json", FileAccess.READ)
			#1:
				#FileAccess.open("res://Dialogue/Emilie_Dialogue1.json", FileAccess.READ)
		if _file == 0:
			var file = FileAccess.open("res://Dialogue/Worker_Dialogue1.json", FileAccess.READ)
			var content = JSON.parse_string(file.get_as_text())
			return content
		else:
			var file = FileAccess.open("res://Dialogue/Emilie_Dialogue1.json", FileAccess.READ)
			var content = JSON.parse_string(file.get_as_text())
			return content

func _input(event):
	if !d_active:
		return
	if event.is_action_pressed("ui_accept"):
		next_script()

func next_script():
	current_dialogue_id += 1
	if current_dialogue_id >= len(dialogue):
		d_active = false
		$NinePatchRect.hide()
		emit_signal("dialogue_finished")
		return
	
	$NinePatchRect/Name.text = dialogue[current_dialogue_id]['name']
	$NinePatchRect/Text.text = dialogue[current_dialogue_id]['text']
