extends Node2D

var enemies: Array = []
var action_queu: Array = []
var is_battling: bool = false
var index: int = 0
var change_scene = preload("res://Scene/FinalScene.tscn")

signal next_player
@onready var choice = $"../CanvasLayer/Choice"

func _ready():
	enemies = get_children()
	for i in enemies.size():
		enemies[i].position = Vector2(0, i*152)
	
	show_choice()

func _process(delta):
	if not choice.visible:
		if Input.is_action_just_pressed("move_up"):
			if index > 0:
				index -= 1
				switch_focus(index, index+1)
				
		if Input.is_action_just_pressed("move_down"):
			if index < enemies.size() -1:
				index += 1
				switch_focus(index, index-1)
				
		if Input.is_action_just_pressed("ui_accept"):
			action_queu.push_back(index)
			emit_signal("next_player")
		if Global.morosCount == 4:
			get_tree().change_scene_to_packed(change_scene)
	
	if action_queu.size() == enemies.size() and not is_battling:
		is_battling = true
		_action(action_queu)

func _action(stack):
	for i in stack:
		enemies[i].take_damage(1)
		await get_tree().create_timer(1).timeout
	action_queu.clear()
	is_battling = false
	show_choice()
		
func switch_focus(x,y):
	enemies[x].focus()
	enemies[y].unfocus()
	
func show_choice():
	choice.show()
	choice.find_child("Attack").grab_focus()

func _reset_focus():
	index = 0
	for enemy in enemies:
		enemy.unfocus()

func _start_choosing():
	_reset_focus()
	enemies[0].focus()

func _on_attack_pressed():
	choice.hide()
	print(enemies.size())
	_start_choosing()
