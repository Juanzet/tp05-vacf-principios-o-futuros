extends CharacterBody2D

@export var speed: int = 400
var axis: Vector2
var accel: int = 100

func _ready():
	pass
#func get_input():
	#var input_direction = Input.get_vector("move_left", "move_rigth", "move_up", "move_down")
	#velocity = input_direction * speed

func _physics_process(delta):
	playerMovement(delta)

func getAxis() -> Vector2:
	axis.x = int(Input.is_action_pressed("move_rigth")) - int(Input.is_action_pressed("move_left"))
	axis.y = int(Input.is_action_pressed("move_down")) - int(Input.is_action_pressed("move_up"))
	return axis
								   
func playerMovement(delta):
	var direction = Vector2(getAxis().x, getAxis().y)
	direction = direction.normalized()
	velocity = velocity.lerp(direction * speed, accel * delta)
	move_and_slide()
													  
