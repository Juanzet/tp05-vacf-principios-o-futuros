extends CharacterBody2D

const SPEED = 30
var current_state = IDLE

var change_scene = preload("res://Prefabs/battle_scene.tscn")

var dir = Vector2.RIGHT
var start_pos

var is_roaming = true
var is_chatting = false

var player
var player_in_chat_zone = true

enum {
	IDLE,
	NEW_DIR,
	MOVE
}

func _ready():
	randomize()
	start_pos = position

func _process(delta):
	if is_roaming:
		match current_state:
			IDLE:
				pass
			NEW_DIR:
				dir = choose([Vector2.RIGHT, Vector2.UP, Vector2.LEFT, Vector2.DOWN])
			MOVE:
				move(delta)
	if Input.is_action_just_pressed("chat"):
		$Dialogue.start()
		print("Chattin with NPC")
		is_roaming = false
		is_chatting = true

func choose(array):
	array.shuffle()
	return array.front()
	
func move(delta):
	if !is_chatting:
		position += dir * SPEED * delta

func _on_chat_detection_area_body_entered(body):
	if body.has_method("Player"):
		player = body
		player_in_chat_zone = true

func _on_chat_detection_area_body_exited(body):
	if body.has_method("Player"):
		player_in_chat_zone = false


func _on_timer_timeout():
	$Timer.wait_time = choose([0.5, 1, 1.5])
	current_state = choose([IDLE, NEW_DIR, MOVE])


func _on_dialogue_dialogue_finished():
	get_tree().change_scene_to_packed(change_scene)
	$Dialogue._file = 1
	is_chatting = false
	is_roaming = true
